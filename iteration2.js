window.onload= function(){ 
    addContent(),
    addP_Content(),
    addsixP_Content(),
    addPWithText_Content(),
    textH2FnInsertHere(),
    insertArray(),
    deleteRnRemoveMe(),
    pTextVoyEnMedio(),
    pVoyDentro()
};

//2.1 Inserta dinamicamente en un html un div vacio con javascript.

function addContent(){
    var newDiv = document.createElement("div");
    var currentDiv = document.getElementById("div1");
    document.body.insertBefore(newDiv, currentDiv);
  
}
//2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.

function addP_Content(){
    var newDiv = document.createElement("div");
    var newP = document.createElement("p");

    newDiv.appendChild(newP);

    var currentDiv = document.getElementById("div1");
    document.body.insertBefore(newDiv, currentDiv);
  
}

//2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.

function addsixP_Content(){
    var newDiv = document.createElement("div");
    
    for(let i=0;i<6;i++){
        //declarar dentro porque si es fuera se borra
        var newP = document.createElement("p");
        newDiv.appendChild(newP);
    }

    var currentDiv = document.getElementById("div1");
    document.body.insertBefore(newDiv, currentDiv);
  
}


//2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

function addPWithText_Content(){
    var newDiv = document.createElement("div");
    var newP = document.createElement("p");
    var content = document.createTextNode("Soy dinámico!");

    newP.appendChild(content);
    newDiv.appendChild(newP);

    var currentDiv = document.getElementById("div1");
    document.body.insertBefore(newDiv, currentDiv);
  
}


//2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.

function textH2FnInsertHere(){
    
    

    var currentDiv = document.querySelector("h2.fn-insert-here");
    var content = document.createTextNode("Wubba Lubba dub dub");
    currentDiv.appendChild(content);


}

//2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.


function insertArray(){

    const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
    var ul = document.createElement("ul");


    for (const value of apps) {

        var li = document.createElement("li");
        var text = document.createTextNode(value);
        li.appendChild(text);
        ul.appendChild(li);
    }
    

    var currentDiv = document.getElementById("div1");
    document.body.insertBefore(ul, currentDiv); 
}


//2.7 Elimina todos los nodos que tengan la clase .fn-remove-me

function deleteRnRemoveMe(){
    var removeElement = document.querySelectorAll(".fn-remove-me");
    console.log(removeElement);  

    removeElement.forEach(element => {
        document.body.removeChild(element);
    });
    
}

//2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
//	Recuerda que no solo puedes insertar elementos con .appendChild.

function pTextVoyEnMedio () {
    var element = document.createElement("p");
    var text = document.createTextNode("Voy en medio!");
    element.appendChild(text);
    var currentDiv = document.querySelectorAll("div")[1];
    document.body.insertBefore(element,currentDiv);
}


//2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here

function pVoyDentro(){
    var divClassFnInsertHere = document.querySelectorAll("div.fn-insert-here");

    divClassFnInsertHere.forEach(element => {
        element.appendChild(document.createElement("p")
                .appendChild(document.createTextNode("Voy dentro!")));
    });

    var currentDiv = document.querySelectorAll("div1")
    document.body.appendChild(element,currentDiv);
}